Source: pyro5
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Bo YU <tsu.yubo@gmail.com>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/python-team/packages/pyro5.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyro5
Homepage: https://github.com/irmen/Pyro5
Rules-Requires-Root: no
Build-Depends: debhelper-compat(= 13), python3-all
Build-Depends-Indep:
 dh-python,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 python3-serpent <!nocheck>,
 python3-setuptools,
 python3-cloudpickle (>= 0.4.0),
 python3-dill (>= 0.2.6),
 python3-msgpack (>= 0.5.2) <!nocheck>,
 python3-pytest <!nocheck>,
 help2man,
Description: distributed object middleware for Python (RPC)
 Pyro (PYthon Remote Object) is an easy to use and powerful distributed object
 system for Python. In a distributed object system, clients send requests to
 distant servers, which owns the remote objects. Pyro simplifies a lot the
 creation of clients and servers, and has among its features:
  - dynamic and static proxies for all remote method invocations,
  - a naming service which keeps record of the location of objects,
  - mobile objects support: clients and servers can pass objects around,
  - exceptions that occur in the remote object is raised on the client too,
  - multithreaded server support to handle multiple requests simultaneously.

Package: python3-pyro5
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: python3-serpent, ${misc:Depends}, ${python3:Depends}
Conflicts: python3-pyro4
Replaces: python3-pyro4
Provides: pyro5
Suggests: pyro5-doc
Description: ${source:Synopsis}
 ${source:Extended-Description}
 .
 This package contains the core Pyro5 module for Python 3.x .
 .
 The documentation is available in the pyro5-doc package.
 A lot of examples are available in the pyro5-examples package.

Package: pyro5-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Suggests: pyro5
Depends: ${misc:Depends}, ${sphinxdoc:Depends}, libjs-jquery, libjs-underscore
Description: ${source:Synopsis}
 ${source:Extended-Description}
 .
 This package contains the documentation files for the pyro5 package.

Package: pyro5-examples
Architecture: all
Depends: ${misc:Depends}, pyro5
Description: ${source:Synopsis}
 ${source:Extended-Description}
 .
 This package contains some examples for the pyro5 package.
